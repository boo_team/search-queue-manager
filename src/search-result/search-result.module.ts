import {Module} from '@nestjs/common';
import {SearchResultController} from './search-result.controller';
import {SearchResultAggregatorService} from './services/search-result-aggregator.service';
import {SearchResultManipulatorService} from './services/search-result-manipulator.service';
import {DbModule} from '../db/db.module';

@Module({
    imports: [
        DbModule,
    ],
    controllers: [
        SearchResultController,
    ],
    providers: [
        SearchResultAggregatorService,
        SearchResultManipulatorService,
    ],
})
export class SearchResultModule {
}
