import {SearchResultSummaryDto} from '../dto/searchResultSummary.dto';
import {HotelSummaryDto} from '../dto/hotelSummary.dto';
import {BuilderType, BuilderWithoutNulls, ToBuilder} from '@bo/common/dist/builder';

export class SearchResultSummaryDtoBuilder {
    public static get = (): BuilderType<SearchResultSummaryDto> =>
        (new SearchResultSummaryDtoToBuilder() as ToBuilder<SearchResultSummaryDto>).toBuilder()
}

@BuilderWithoutNulls
class SearchResultSummaryDtoToBuilder implements SearchResultSummaryDto {
    public searchId: string = null;
    public firstCreatedAt: string = null;
    public lastCreatedAt: string = null;
    public avgSearchProcessTimeSeconds: number = null;
    public lastSearchProcessTimeSeconds: number = null;
    public avgScrapingTimeSeconds: number = null;
    public lastScrapingTimeSeconds: number = null;
    public resultsCount: number = null;
    public hotelsSummary: HotelSummaryDto[] = null;
}
