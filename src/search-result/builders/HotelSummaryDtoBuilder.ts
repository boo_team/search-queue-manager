import {HotelSummaryDto} from '../dto/hotelSummary.dto';
import {CoordsDto} from '../dto/coords.dto';
import {ValueWithDate} from '../interface/valueWithDate';
import {BuilderType, BuilderWithoutNulls, ToBuilder} from '@bo/common/dist/builder';
import {BonusesDto} from '../dto/bonuses.dto';

export class HotelSummaryDtoBuilder {
    public static get = (): BuilderType<HotelSummaryDto> =>
        (new HotelSummaryDtoToBuilder() as ToBuilder<HotelSummaryDto>).toBuilder()
}

@BuilderWithoutNulls
class HotelSummaryDtoToBuilder implements HotelSummaryDto {
    public hotelId: string = null;
    public name: string = null;
    public distanceFromCenterMeters: number = null;
    public districtName: string = null;
    public coords: CoordsDto = null;
    public hotelLink: string = null;
    public propertyType: string = null;
    public starRating: number = null;
    public newlyAdded: boolean = null;
    public lastPrice: number = null;
    public lastRate: number = null;
    public lastSecondaryRateType: string = null;
    public lastSecondaryRate: number = null;
    public lastPriceBeforeDiscount: number = null;
    public lastNumberOfReviews: number = null;
    public lastBonuses: BonusesDto = null;
    public lastBonusesOld: string = null;
    public lastRooms: string = null;
    public avgPrice: number = null;
    public minPrice: number = null;
    public maxPrice: number = null;
    public avgPriceDiff: number = null;
    public maxPriceDiff: number = null;
    public prices: ValueWithDate<number>[] = null;
}
