import {BuilderType, BuilderWithoutNulls, ToBuilder} from '@bo/common/dist/builder';
import {RoomRaw} from '../interface/room.raw';

export class RoomRawBuilder {
    public static get = (): BuilderType<RoomRaw> =>
        (new RoomRawToBuilder() as ToBuilder<RoomRaw>).toBuilder()
}

@BuilderWithoutNulls
class RoomRawToBuilder implements RoomRaw {
    public description: string = null;
    public personCount: string = null;
    public beds: string = null;
}
