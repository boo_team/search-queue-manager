import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Model, QueryCursor} from 'mongoose';
import {SearchResultAggregatorService} from './search-result-aggregator.service';
import {SearchResultSimpleSummary} from '../dto/searchResultSimpleSummary.dto';
import {SearchResultSummaryDto} from '../dto/searchResultSummary.dto';
import {RawSearchResultDocument, RawSearchResultSchemaKey} from '@bo/common';

@Injectable()
export class SearchResultManipulatorService {

    constructor(@InjectModel(RawSearchResultSchemaKey) private readonly rawSearchResultModel: Model<RawSearchResultDocument>,
                private readonly searchResultAggregatorService: SearchResultAggregatorService) {
    }

    public async getAllSearchResultsSummary(): Promise<{ searchId: string, summary: SearchResultSimpleSummary[] }[]> {
        const searchResultQueryCursor: QueryCursor<RawSearchResultDocument> = this.rawSearchResultModel.find({}, {hotels: 0}).cursor();
        const searchResults: { [searchId: string]: SearchResultSimpleSummary[] } = {};
        await searchResultQueryCursor.eachAsync(
            ({searchId, _id, createdAt, scrapingTimeSeconds, searchProcessTimeSeconds, hotelsCount}: RawSearchResultDocument) => {
                const summary = {
                    _id,
                    createdAt,
                    scrapingTimeSeconds,
                    searchProcessTimeSeconds,
                    hotelsCount,
                };
                if (searchResults[searchId]) {
                    searchResults[searchId].push(summary);
                } else {
                    searchResults[searchId] = [summary];
                }
            });
        return Object.keys(searchResults).map((v: string) => ({searchId: v, summary: searchResults[v]}));
    }

    public async getSearchResultSummary(searchId: string): Promise<SearchResultSummaryDto | null> {
        const searchResultQueryCursor: QueryCursor<RawSearchResultDocument> = await this.rawSearchResultModel.find({searchId}).cursor();
        return this.searchResultAggregatorService.getSearchResultSummary(searchResultQueryCursor);
    }

    // TODO: move to search-results-calculator and invoke on StatusEvent.SEARCH_REQUEST_DELETED
    public async delete(searchId: string): Promise<{ n?: number }> {
        return this.rawSearchResultModel.deleteMany({searchId}).exec();
    }
}
