import {Controller, Get, Param} from '@nestjs/common';
import {SearchResultManipulatorService} from './services/search-result-manipulator.service';
import {SearchResultSimpleSummary} from './dto/searchResultSimpleSummary.dto';
import {Roles} from '../common/decorator/roles.decorator';
import {Role} from '../users/model/Role';
import {SearchResultSummaryDto} from './dto/searchResultSummary.dto';

@Controller('api/v1/search-results')
export class SearchResultController {

    constructor(private readonly searchResultManipulatorService: SearchResultManipulatorService) {
    }

    @Get('/:searchId')
    @Roles(Role.USER)
    public async getSearchResult(@Param('searchId') searchId: string): Promise<SearchResultSummaryDto> {
        return await this.searchResultManipulatorService.getSearchResultSummary(searchId);
    }

    @Get()
    @Roles(Role.ADMIN)
    public async getAllSearchResultsSummary(): Promise<{ searchId: string, summary: SearchResultSimpleSummary[] }[]> {
        return await this.searchResultManipulatorService.getAllSearchResultsSummary();
    }
}
