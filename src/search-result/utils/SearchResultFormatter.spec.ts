import {SearchResultFormatter} from './SearchResultFormatter';

describe('SearchResultFormatter: ', () => {

    let formatter: SearchResultFormatter;

    beforeEach(() => {
        formatter = new SearchResultFormatter();
    });

    describe('processDistance():', () => {
        it('should parse distance to meters [1]', () => {
            // Prepare
            const test = '150 m from centre';

            // Verify
            expect(formatter.processDistance(test)).toBe(150);
        });

        it('should parse distance to meters [2]', () => {
            // Prepare
            const test = '1 km from centre';

            // Verify
            expect(formatter.processDistance(test)).toBe(1000);
        });

        it('should parse distance to meters [3]', () => {
            // Prepare
            const test = '1.8 km from centre';

            // Verify
            expect(formatter.processDistance(test)).toBe(1800);
        });

        it('should parse distance to meters [4]', () => {
            // Prepare
            const test = '12.9 km from centre';

            // Verify
            expect(formatter.processDistance(test)).toBe(12900);
        });

        it('should parse distance to meters [5]', () => {
            // Prepare
            const test = '150 feet from centre';

            // Verify
            expect(formatter.processDistance(test)).toBe(46);
        });

        it('should parse distance to meters [6]', () => {
            // Prepare
            const test = '2,450 feet from centre';

            // Verify
            expect(formatter.processDistance(test)).toBe(747);
        });

        it('should parse distance to meters [7]', () => {
            // Prepare
            const test = '1 miles from centre';

            // Verify
            expect(formatter.processDistance(test)).toBe(1609);
        });

        it('should parse distance to meters [8]', () => {
            // Prepare
            const test = '1.8 miles from centre';

            // Verify
            expect(formatter.processDistance(test)).toBe(2897);
        });

        it('should parse distance to meters [9]', () => {
            // Prepare
            const test = '12.9 miles from centre';

            // Verify
            expect(formatter.processDistance(test)).toBe(20761);
        });

        it('should parse distance to meters [10]', () => {
            // Prepare
            const test = undefined;

            // Verify
            expect(formatter.processDistance(test)).toBeNull();
        });

        it('should parse distance to meters [11]', () => {
            // Prepare
            const test = '';

            // Verify
            expect(formatter.processDistance(test)).toBeNull();
        });
    });

    describe('processDistrictName():', () => {
        it('should parse district name [1]', () => {
            // Prepare
            const test = 'Eixample,Barcelona – Show on map';

            // Verify
            expect(formatter.processDistrictName(test)).toBe('Eixample');
        });

        it('should parse district name [2]', () => {
            // Prepare
            const test = 'Upper West Side,New York City – Show on map';

            // Verify
            expect(formatter.processDistrictName(test)).toBe('Upper West Side');
        });

        it('should parse district name [3]', () => {
            // Prepare
            const test = 'Śródmieście,Gdynia – Show on map"';

            // Verify
            expect(formatter.processDistrictName(test)).toBe('Śródmieście');
        });

        it('should parse district name [4]', () => {
            // Prepare
            const test = 'Śródmieście, Pogórze, Slisko,Gdynia – Show on map';

            // Verify
            expect(formatter.processDistrictName(test)).toBe('Śródmieście Pogórze Slisko');
        });

        it('should parse district name [5]', () => {
            // Prepare
            const test = ' – Show on map';

            // Verify
            expect(formatter.processDistrictName(test)).toBeNull();
        });

        it('should parse district name [6]', () => {
            // Prepare
            const test = '';

            // Verify
            expect(formatter.processDistrictName(test)).toBeNull();
        });
    });

    describe('processCoords():', () => {
        it('should parse coords [1]', () => {
            // Prepare
            const test = '19.937436,50.059889';

            // Verify
            expect(formatter.processCoords(test)).toEqual({lat: 50.059889, lon: 19.937436});
        });

        it('should parse coords [2]', () => {
            // Prepare
            const test = '121.707012653278,31.1850818301733';

            // Verify
            expect(formatter.processCoords(test)).toEqual({lat: 31.1850818301733, lon: 121.707012653278});
        });
    });
});
