import {CoordsDto} from '../dto/coords.dto';
import {RoomRaw} from '../interface/room.raw';
import {BonusesDto} from '../dto/bonuses.dto';
import {logger} from '../../config';

export class SearchResultFormatter {

    public parseRate(rate: string): number | null {
        if (rate && rate.trim() === '10') { // It is only one case when there is no dot
            return 100;
        }
        if (rate) {
            const rateWithoutDots = rate.trim().replace(/\./g, '');
            return parseInt(rateWithoutDots, 0) || null;
        } else {
            return null;
        }
    }

    public parseNumberOfReviews = (numberOfReviews: string): number | null => parseInt(this.removeComma(numberOfReviews), 0) || null;

    public processDistance(distance: string): number | null {
        if (distance && distance.length > 0) {
            const distanceArray = distance.split(' ');
            const value = distanceArray[0].replace(/,/, '.');
            const unit = distanceArray[1];
            // km to meters conversion
            if (unit && unit.includes('km')) {
                return parseFloat(value) * 1000;
            }
            // feet to meters conversion
            if (unit && unit.includes('feet')) {
                return value.includes('.')
                    ? Math.round(parseFloat(value) * 1000 * 0.3048)
                    : Math.round(parseFloat(value) * 0.3048);
            }
            // miles to meters conversion
            if (unit && unit.includes('miles')) {
                return Math.round(parseFloat(value) * 1609.344);
            }
            // meters
            const parsedValue = parseInt(value, 0);
            return isFinite(parsedValue) ? parsedValue : null;
        } else {
            return null;
        }
    }

    // "Eixample,Barcelona – Show on map"
    // "Upper West Side,New York City – Show on map"
    // "Śródmieście,Gdynia – Show on map"
    public processDistrictName(districtName: string): string | null {
        if (!districtName) {
            return null;
        }
        const districtNameWithCity = districtName.replace(/ –([^–]+)$/g, '').trim();
        const splitArr = districtNameWithCity.split(',');
        splitArr.pop();
        return (splitArr && splitArr.length > 0)
            ? splitArr.reduce((v1, v2) => v1 + v2)
            : null;
    }

    // ORDER: lon, lat
    // "19.937436,50.059889"
    // "121.707012653278,31.1850818301733"
    public processCoords(coords: string): CoordsDto | null {
        if (!coords) {
            return null;
        }
        const split = coords.split(',');
        return {
            lat: Number(split[1]),
            lon: Number(split[0]),
        };
    }

    public parseStarRating(starRating: string): number | null {
        if (starRating && starRating.length > 0) {
            const starNumber: string = starRating.split('-')[0];
            const parsedValue = parseInt(starNumber, 0);
            return isNaN(parsedValue) ? null : parsedValue;
        } else {
            return null;
        }
    }

    public parseBonuses(bonuses: string[]): BonusesDto {
        return bonuses && bonuses.length
            ? this.mapBonuses(bonuses)
            : null;
    }

    public parseBonusesOld(bonuses: string[]): string {
        return bonuses && bonuses.length
            ? bonuses.map(bonus => this.mapBonusOld(bonus)).filter((v) => !!v).join(', ')
            : null;
    }

    public parseRooms(rooms: RoomRaw[]): string {
        return rooms && rooms.length
            ? rooms.map(bonus => this.formatRoom(bonus)).join(', ')
            : null;
    }

    private mapBonuses(bonuses: string[]): BonusesDto {
        return {
            freeCancellation: bonuses.some((v) => v.toLowerCase().includes('free cancellation')),
            cancelLater: bonuses.some((v) => v.toLowerCase().includes('risk free')),
            noPrepayment: bonuses.some((v) => v.toLowerCase().includes('no prepayment')),
            breakfastIncluded: bonuses.some((v) => v.toLowerCase().includes('breakfast included')),
        };
    }

    private mapBonusOld(bonus: string): string {
        if (bonus.toLowerCase().includes('risk free')) {
            return 'Cancel later';
        }
        if (bonus.toLowerCase().includes('free cancellation')) {
            return 'Free cancellation';
        }
        if (bonus.toLowerCase().includes('no prepayment')) {
            return 'No prepayment';
        }
        if (bonus.toLowerCase().includes('breakfast included')) {
            return 'Breakfast included';
        }
        logger.error('No map for bonus : ' + bonus);
        return null;
    }

    private formatRoom(room: RoomRaw): string {
        let text = `${room.description}`;
        if (room.personCount) {
            text = text.concat(` (${room.personCount})`);
        }
        if (room.beds) {
            text = text.concat(` (${room.beds})`);
        }
        return text;
    }

    private removeComma = (value: string) => value ? value.replace(/,/g, '') : null;
}
