export interface RoomRaw {
    readonly description: string;
    readonly personCount: string;
    readonly beds: string | null;
}
