import { RawHotelDocument } from '@bo/common';

export interface HotelMatcher {
    readonly idx: number;
    readonly name: string;
    coords: string | null;
    distanceFromCenter: string | null;
    districtName: string | null;
    readonly hotelLink: string;
    readonly hotelByDate: Map<string, RawHotelDocument>;
}
