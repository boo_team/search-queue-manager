export interface SearchResultDetails {
    readonly searchId: string;
    readonly createdAt: string;
    readonly scrapingTimeSeconds: number;
    readonly searchProcessTimeSeconds: number;
}
