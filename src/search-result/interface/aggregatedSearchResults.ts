import {SearchResultDetails} from './searchResultDetails';
import {RawHotelDocument} from '@bo/common';

export interface AggregatedSearchResults {
    readonly hotelsByDate: Map<string, RawHotelDocument>[];
    readonly searchResultsDetails: SearchResultDetails[];
}
