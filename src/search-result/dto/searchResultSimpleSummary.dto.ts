import {ObjectId} from 'bson';

export interface SearchResultSimpleSummary {
    readonly _id: ObjectId;
    readonly createdAt: string;
    readonly scrapingTimeSeconds: number;
    readonly searchProcessTimeSeconds: number;
    readonly hotelsCount: number;
}
