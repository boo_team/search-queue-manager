import {SearchRequestBase} from '../interface/base/searchRequest.base';
import { SearchRequestDocument } from '@bo/common';

export interface SearchRequestDto extends SearchRequestBase, Pick<SearchRequestDocument, 'updatedAt' | 'createdAt'> {
}
