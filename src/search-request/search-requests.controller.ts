import {Body, ConflictException, Controller, Delete, Get, HttpStatus, Param, Post, Put, UsePipes} from '@nestjs/common';
import {SearchRequestsService} from './search-requests.service';
import {SearchRequestValidatorPipe} from './middlewares/search-request-validator.pipe';
import {SearchRequestFactory} from './utils/SearchRequestFactory';
import {SearchRequestMapper} from './utils/SearchRequestMapper';
import {Roles} from '../common/decorator/roles.decorator';
import {Role} from '../users/model/Role';
import {SearchRequestsRepository} from './search-requests.repository';
import {AuthClientService} from '../auth-client/auth-client.service';
import {OwnedRequestsService} from './client/owned-requests.service';
import {Limited} from '../common/decorator/limited.decorator';
import {UserIdFromToken} from '../common/decorator/user-id-from-token.decorator';
import {SearchRequestDto} from './dto/searchRequest.dto';
import {SearchRequestRaw} from './interface/searchRequest.raw';
import {WebService} from '../web/web.service';

@Controller('api/v1/search-requests')
export class SearchRequestsController {

    constructor(private readonly authClientService: AuthClientService,
                private readonly ownedRequestsService: OwnedRequestsService,
                private readonly searchRequestsService: SearchRequestsService,
                private readonly searchRequestsRepository: SearchRequestsRepository,
                private readonly webService: WebService) {
    }

    @Get(':searchId')
    @Roles(Role.USER)
    public async getSearchRequest(@Param('searchId') searchId: string): Promise<SearchRequestDto> {
        const searchRequest = await this.searchRequestsRepository.getOrFail(searchId);
        return SearchRequestMapper.fromDocToDto(searchRequest);
    }

    @Post()
    @Roles(Role.USER)
    @Limited()
    @UsePipes(new SearchRequestValidatorPipe())
    public async createSearchRequest(@UserIdFromToken() userId: string,
                                     @Body() searchRequestRaw: SearchRequestRaw): Promise<SearchRequestDto> {
        const userDoc = await this.authClientService.findUserById(userId);
        const searchRequest = new SearchRequestFactory().build(searchRequestRaw);
        const foundInUser = userDoc.ownedSearchRequests.some((v) => v.searchId.includes(searchRequest.searchId));
        if (foundInUser) {
            throw new ConflictException('User already has this search request. Try to create a request for different conditions.');
        }

        const foundOrCreated = await this.searchRequestsService.findOrCreate(searchRequest);
        const updatedUserDoc = await this.ownedRequestsService.add(userDoc, foundOrCreated.searchId);
        const allRequestsFromUserDocs = await this.searchRequestsRepository.getAllFromUser(updatedUserDoc);
        this.webService.sendSearchRequestsToUserClients(userDoc, allRequestsFromUserDocs);

        return SearchRequestMapper.fromDocToDto(foundOrCreated);
    }

    @Put(':oldSearchId')
    @Roles(Role.USER)
    @UsePipes(new SearchRequestValidatorPipe())
    public async updateSearchRequest(@UserIdFromToken() userId: string,
                                     @Param('oldSearchId') oldSearchId: string,
                                     @Body() searchRequestRaw: SearchRequestRaw): Promise<SearchRequestDto> {
        const userDoc = await this.authClientService.findUserById(userId);
        const searchRequest = new SearchRequestFactory().build(searchRequestRaw);
        const foundInUser = userDoc.ownedSearchRequests.some((v) => v.searchId.includes(searchRequest.searchId));
        if (foundInUser) {
            throw new ConflictException('User already has this search request. Try to create a request for different conditions.');
        }

        await this.searchRequestsService.deleteReqAndResultsIfNotFoundInOtherUsers(userDoc, oldSearchId);
        const foundOrCreated = await this.searchRequestsService.findOrCreate(searchRequest);
        const updatedUserDoc = await this.ownedRequestsService.update(userDoc, oldSearchId, foundOrCreated.searchId);
        const allRequestsFromUserDocs = await this.searchRequestsRepository.getAllFromUser(updatedUserDoc);
        this.webService.sendSearchRequestsToUserClients(userDoc, allRequestsFromUserDocs);

        return SearchRequestMapper.fromDocToDto(foundOrCreated);
    }

    @Delete(':searchId')
    @Roles(Role.USER)
    public async deleteSearchRequest(@UserIdFromToken() userId: string,
                                     @Param('searchId') searchId: string): Promise<HttpStatus> {
        const userDoc = await this.authClientService.findUserById(userId);
        await this.searchRequestsService.deleteReqAndResultsIfNotFoundInOtherUsers(userDoc, searchId);
        const updatedUserDoc = await this.ownedRequestsService.delete(userDoc, searchId);
        const allRequestsFromUserDocs = await this.searchRequestsRepository.getAllFromUser(updatedUserDoc);
        this.webService.sendSearchRequestsToUserClients(userDoc, allRequestsFromUserDocs);

        return HttpStatus.OK;
    }

    @Get()
    @Roles(Role.ADMIN)
    public async getAllSearchRequests(): Promise<SearchRequestDto[]> {
        const allSearchRequestsDoc = await this.searchRequestsRepository.getAll();
        return allSearchRequestsDoc.map((v) => SearchRequestMapper.fromDocToDto(v));
    }
}
