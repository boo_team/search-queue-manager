import {Injectable} from '@nestjs/common';
import {AuthClientService} from '../../auth-client/auth-client.service';
import {logger} from '../../config';
import {ObjectId} from 'bson';
import {OwnedRequestDocument, UserDocument} from '@bo/common';

@Injectable()
export class OwnedRequestsService {

    constructor(private readonly authClientService: AuthClientService) {
    }

    public async add({_id, ownedSearchRequests}: UserDocument, newSearchId: string): Promise<UserDocument> {
        const updatedOwnedReqs = ownedSearchRequests.slice();
        updatedOwnedReqs.push({searchId: newSearchId, favorites: []} as OwnedRequestDocument);
        return this.updateDoc(_id, updatedOwnedReqs);
    }

    public async update({_id, ownedSearchRequests}: UserDocument, oldSearchId: string, newSearchId: string): Promise<UserDocument> {
        const updatedOwnedReqs = ownedSearchRequests.filter((v) => v.searchId !== oldSearchId);
        updatedOwnedReqs.push({searchId: newSearchId, favorites: []} as OwnedRequestDocument);
        return this.updateDoc(_id, updatedOwnedReqs);
    }

    public async delete({_id, ownedSearchRequests}: UserDocument, oldSearchId: string): Promise<UserDocument> {
        const updatedOwnedReqs = ownedSearchRequests.filter((v) => v.searchId !== oldSearchId);
        return this.updateDoc(_id, updatedOwnedReqs);
    }

    private async updateDoc(userId: ObjectId, updatedOwnedReqs: OwnedRequestDocument[]): Promise<UserDocument> {
        const updatedUserDoc = await this.authClientService.findByIdAndUpdateOwnedReq(userId, updatedOwnedReqs);
        logger.debug(`User: [${updatedUserDoc.email}] was updated by owned search requests: `, updatedUserDoc.ownedSearchRequests);
        return updatedUserDoc;
    }

    public async isOtherUsersOwnsReq({_id}: UserDocument, searchId: string): Promise<boolean> {
        const userDocs = await this.authClientService.findOtherUsersWhoOwnReq(_id, searchId);
        return userDocs && userDocs.length > 0;
    }

    public async findOwnersOfRequests(searchIds: string[]): Promise<UserDocument[]> {
        const foundUsers: Map<string, UserDocument> = new Map();
        for (const searchId of searchIds) {
            const users: UserDocument[] = await this.authClientService.findOwnersOfSearchId(searchId);
            users.forEach((u) => foundUsers.set(u._id.toString(), u));
        }
        return Array.from(foundUsers.values());
    }
}
