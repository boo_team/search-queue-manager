import {InjectModel} from '@nestjs/mongoose';
import {Model} from 'mongoose';
import {Injectable, NotFoundException} from '@nestjs/common';
import {SearchRequestBase} from './interface/base/searchRequest.base';
import {SearchRequestDocument, SearchRequestSchemaKey, UserDocument} from '@bo/common';

@Injectable()
export class SearchRequestsRepository {

    constructor(@InjectModel(SearchRequestSchemaKey) private readonly searchRequestModel: Model<SearchRequestDocument>) {
    }

    public getOrFail(searchId: string): Promise<SearchRequestDocument> {
        return this.searchRequestModel.findOne({searchId})
            .orFail(() => new NotFoundException(`Search request with search id: ${searchId} not exist`)).exec();
    }

    public get(searchId: string): Promise<SearchRequestDocument> {
        return this.searchRequestModel.findOne({searchId}).exec();
    }

    public create(searchRequest: SearchRequestBase): Promise<SearchRequestDocument> {
        return new this.searchRequestModel(searchRequest).save();
    }

    public delete(searchId: string): Promise<SearchRequestDocument> | null {
        return this.searchRequestModel.findOneAndDelete({searchId})
            .orFail(() => new NotFoundException(`Search request with search id: ${searchId} not exist`)).exec();
    }

    public getAllFromUser(userDoc: UserDocument): Promise<SearchRequestDocument[]> {
        const ownedSearchRequestsId = userDoc.ownedSearchRequests.map(v => v.searchId);
        return this.searchRequestModel.find({searchId: {$in: ownedSearchRequestsId}}).exec();
    }

    public getAll(): Promise<SearchRequestDocument[]> {
        return this.searchRequestModel.find().exec();
    }
}
