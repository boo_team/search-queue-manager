import {Module} from '@nestjs/common';
import {SearchRequestsService} from './search-requests.service';
import {SearchRequestsController} from './search-requests.controller';
import {SearchRequestsRepository} from './search-requests.repository';
import {SearchRequestValidatorPipe} from './middlewares/search-request-validator.pipe';
import {OwnedRequestsService} from './client/owned-requests.service';
import {StatusModule} from '../status/status.module';
import {WebModule} from '../web/web.module';
import {AuthClientModule} from '../auth-client/auth-client.module';
import {DbModule} from '../db/db.module';

@Module({
    imports: [
        StatusModule,
        WebModule,
        AuthClientModule,
        DbModule,
    ],
    controllers: [
        SearchRequestsController,
    ],
    providers: [
        SearchRequestsRepository,
        SearchRequestsService,
        SearchRequestValidatorPipe,
        OwnedRequestsService,
    ],
})
export class SearchRequestsModule {
}
