import {Injectable} from '@nestjs/common';
import {SearchRequestsRepository} from './search-requests.repository';
import {OwnedRequestsService} from './client/owned-requests.service';
import {SearchRequestBase} from './interface/base/searchRequest.base';
import {SearchRequestDocument, UserDocument, SocketClient, StatusEvent, SearchRequestStatusDto} from '@bo/common';
import {logger} from '../config';
import {WebService} from '../web/web.service';
import {ConnectedEvent} from '../web/interfaces/connected.event';

@Injectable()
export class SearchRequestsService {

    constructor(
        private readonly ownedRequestsService: OwnedRequestsService,
        private readonly searchRequestsRepository: SearchRequestsRepository,
        private readonly socketClient: SocketClient,
        private readonly webService: WebService,
    ) {
        this.watchEvents();
    }

    private watchEvents(): void {
        this.socketClient.watchEvent<SearchRequestStatusDto>(StatusEvent.SEARCH_REQUEST_UPDATE)
            .subscribe(async (dto: SearchRequestStatusDto) => {
                logger.debug(`[${StatusEvent.SEARCH_REQUEST_UPDATE}] event received from status monitor. Dto:`, dto);
                const owners = await this.ownedRequestsService.findOwnersOfRequests([dto.searchId]);
                for (const userDoc of owners) {
                    const allRequestsFromUserDocs = await this.searchRequestsRepository.getAllFromUser(userDoc);
                    this.webService.sendSearchRequestsToUserClients(userDoc, allRequestsFromUserDocs);
                }
            });
        this.webService.clientConnected$().subscribe(async ({client, userDoc}: ConnectedEvent) => {
            const allRequestsFromUserDocs = await this.searchRequestsRepository.getAllFromUser(userDoc);
            this.webService.sendSearchRequestsToClient(client, allRequestsFromUserDocs);
        });
    }

    public async findOrCreate(searchRequest: SearchRequestBase): Promise<SearchRequestDocument> {
        const found = await this.searchRequestsRepository.get(searchRequest.searchId);
        const foundOrCreated = found
            ? found
            : await this.searchRequestsRepository.create(searchRequest);
        if (!found) {
            logger.info('Search request was created: ', foundOrCreated);
            this.socketClient.sendIfConnected<SearchRequestStatusDto>(StatusEvent.NEW_SEARCH_REQUEST,
                {
                    searchId: foundOrCreated.searchId,
                    timestamp: Date.now(),
                },
            );
        }
        return foundOrCreated;
    }

    public async deleteReqAndResultsIfNotFoundInOtherUsers(userDoc: UserDocument, searchId: string): Promise<void> {
        const foundInOtherUser = await this.ownedRequestsService.isOtherUsersOwnsReq(userDoc, searchId);
        if (!foundInOtherUser) {
            const deletedReq = await this.searchRequestsRepository.delete(searchId);
            logger.info('Search request was deleted: ', deletedReq);
            this.socketClient.sendIfConnected<SearchRequestStatusDto>(StatusEvent.SEARCH_REQUEST_DELETED,
                {
                    searchId: deletedReq.searchId,
                    timestamp: Date.now(),
                },
            );
        }
    }
}
