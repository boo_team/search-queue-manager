import { SearchRequestDocument } from '@bo/common';
import {SearchRequestDto} from '../dto/searchRequest.dto';

export class SearchRequestMapper {
    public static fromDocToDto({
                                   searchId,
                                   priority,
                                   updateFrequencyMinutes,
                                   resultsLimit,
                                   occupancyStatus,
                                   occupancyUpdatedAt,
                                   updatedAt,
                                   createdAt,
                                   city,
                                   checkInDate: checkInDayDoc,
                                   checkOutDate: checkOutDayDoc,
                                   numberOfRooms,
                                   numberOfAdults,
                                   numberOfChildren: numberOfChildrenDoc,
                                   currency,
                                   language,
                               }: SearchRequestDocument): SearchRequestDto {
        return {
            searchId,
            priority,
            updateFrequencyMinutes,
            resultsLimit,
            occupancyStatus,
            occupancyUpdatedAt,
            updatedAt,
            createdAt,
            city,
            checkInDate: {
                day: checkInDayDoc.day,
                month: checkInDayDoc.month,
                year: checkInDayDoc.year,
            },
            checkOutDate: {
                day: checkOutDayDoc.day,
                month: checkOutDayDoc.month,
                year: checkOutDayDoc.year,
            },
            numberOfRooms,
            numberOfAdults,
            numberOfChildren: numberOfChildrenDoc.map((v) => ({yearAgeAtCheckOut: v.yearAgeAtCheckOut})),
            currency,
            language,
        };
    }
}
