import {CheckDateBuilder} from './CheckDateBuilder';
import {SearchRequestRaw} from '../interface/searchRequest.raw';
import {Builder, BuilderType, ToBuilder} from '@bo/common/dist/builder';
import {Currency} from '../enum/Currency';
import {Language} from '../enum/Language';
import {ChildrenPropertiesBase} from '../interface/base/childrenProperties.base';
import {CheckDateBase} from '../interface/base/checkDate.base';

export class SearchRequestRawBuilder {
    public static get = (): BuilderType<SearchRequestRaw> =>
        (new SearchRequestToBuilder() as ToBuilder<SearchRequestRaw>).toBuilder()

    public static fullyPopulatedBuilder = (): BuilderType<SearchRequestRaw> =>
        (new SearchRequestToBuilder() as ToBuilder<SearchRequestRaw>)
            .toBuilder()
            .priority(0)
            .resultsLimit(100)
            .updateFrequencyMinutes(60)
            .city('Warszawa')
            .checkInDate(CheckDateBuilder.fullyPopulatedBuilder()
                .build())
            .checkOutDate(CheckDateBuilder.fullyPopulatedBuilder()
                .day(3)
                .build())
            .numberOfRooms(1)
            .numberOfAdults(2)
            .numberOfChildren([])
            .currency(Currency.PLN)
            .language(Language.ENGLISH)
}

@Builder
class SearchRequestToBuilder implements SearchRequestRaw {
    public priority: number = null;
    public resultsLimit: number = null;
    public updateFrequencyMinutes: number = null;
    public city: string = null;
    public checkInDate: CheckDateBase = null;
    public checkOutDate: CheckDateBase = null;
    public numberOfRooms: number = null;
    public numberOfAdults: number = null;
    public numberOfChildren: ChildrenPropertiesBase[] = null;
    public currency: Currency = null;
    public language: Language = null;
}
