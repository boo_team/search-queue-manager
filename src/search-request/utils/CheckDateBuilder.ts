import {CheckDateBase} from '../interface/base/checkDate.base';
import {Builder, BuilderType, ToBuilder} from '@bo/common/dist/builder';

export class CheckDateBuilder {

    public static get = (): BuilderType<CheckDateBase> =>
        (new CheckDateToBuilder() as ToBuilder<CheckDateBase>).toBuilder()

    public static fullyPopulatedBuilder = (): BuilderType<CheckDateBase> =>
        (new CheckDateToBuilder() as ToBuilder<CheckDateBase>).toBuilder()
            .day(1)
            .month(1)
            .year(2018)
}

@Builder
class CheckDateToBuilder implements CheckDateBase {
    public day: number = null;
    public month: number = null;
    public year: number = null;
}
