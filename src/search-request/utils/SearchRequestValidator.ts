import {isNumber, isObject, isString, isUndefined} from 'lodash';
import {SearchRequestRaw} from '../interface/searchRequest.raw';
import { SearchRequestDocument } from '@bo/common';
import {CheckDateBase} from '../interface/base/checkDate.base';
import {Currency} from '../enum/Currency';
import {Language} from '../enum/Language';

type RawRequest = SearchRequestRaw | SearchRequestDocument;

export class SearchRequestValidator {

    private readonly searchRequestRelevantKeys: Array<keyof RawRequest> = [
        'priority', 'updateFrequencyMinutes', 'resultsLimit', 'city', 'checkInDate', 'checkOutDate', 'numberOfRooms',
        'numberOfAdults', 'numberOfChildren', 'currency', 'language'];

    public validate(searchRequestRaw: RawRequest, now: Date): void | Error {
        let errorMessages = [];
        errorMessages = errorMessages.concat(this.allRelevantValuesDefined(searchRequestRaw));
        if (errorMessages.length < 1) {
            errorMessages = errorMessages.concat(this.hasNumberValues(searchRequestRaw));
            errorMessages = errorMessages.concat(this.validateCityAndDate(searchRequestRaw, now));
            errorMessages = errorMessages.concat(this.validateCurrencyAndLanguage(searchRequestRaw));
        }
        if (errorMessages.length > 0) {
            throw new Error(this.concatErrorMessagesIntoOneMessage(errorMessages));
        }
    }

    public validateOnlyDate({checkInDate, checkOutDate}: RawRequest, now: Date = new Date()): void | Error {
        let errorMessages = this.validateDate(checkInDate, checkOutDate);
        errorMessages = errorMessages.concat(this.isNotInPast(checkInDate, now));
        if (errorMessages.length > 0) {
            throw new Error(this.concatErrorMessagesIntoOneMessage(errorMessages));
        }
    }

    private allRelevantValuesDefined(searchRequestRaw: RawRequest): string[] {
        const undefinedValues = this.searchRequestRelevantKeys
            .filter((v) => isUndefined(searchRequestRaw[v]) || searchRequestRaw[v] === null);
        return undefinedValues
            ? undefinedValues.map((v) => `Value of '${String(v)}' should be defined.`)
            : [];
    }

    private hasNumberValues({
                                priority,
                                updateFrequencyMinutes,
                                resultsLimit,
                                numberOfRooms,
                                numberOfAdults,
                            }: RawRequest): string[] {
        const errorMessages = [];
        if (!isNumber(priority)) {
            errorMessages.push(`'priority' should be a number value, not: '${priority}'`);
        }
        if (!isNumber(updateFrequencyMinutes)) {
            errorMessages.push(`'updateFrequencyMinutes' should be a number value, not: '${updateFrequencyMinutes}'`);
        }
        if (!isNumber(resultsLimit)) {
            errorMessages.push(`'resultsLimit' should be a number value, not: '${resultsLimit}'`);
        }
        if (!isNumber(numberOfRooms)) {
            errorMessages.push(`'numberOfRooms' should be a number value, not: '${numberOfRooms}'`);
        }
        if (!isNumber(numberOfAdults)) {
            errorMessages.push(`'numberOfAdults' should be a number value, not: '${numberOfAdults}'`);
        }
        return errorMessages;
    }

    private validateCityAndDate({
                                    city,
                                    checkInDate,
                                    checkOutDate,
                                }: RawRequest,
                                now: Date): string[] {
        let errorMessages = [];
        if (!city || !isString(city)) {
            errorMessages.push(`'city' should exist and be a string value, not: '${city}'`);
        }
        errorMessages = errorMessages.concat(this.dateHasOnlyNumbers(checkInDate));
        errorMessages = errorMessages.concat(this.dateHasOnlyNumbers(checkOutDate));
        errorMessages = errorMessages.concat(this.isNotInPast(checkInDate, now));
        if (errorMessages.length === 0) {
            errorMessages = errorMessages.concat(this.validateDate(checkInDate, checkOutDate));
        }
        return errorMessages;
    }

    private dateHasOnlyNumbers(searchDate: CheckDateBase): string[] {
        const errorMessages = [];
        if (!searchDate || !isObject(searchDate)) {
            errorMessages.push(`'searchDate' should exist and be an object`);
        } else {
            const {day, month, year} = searchDate;
            if (!day || !isNumber(day)) {
                errorMessages.push(`'day' should exist and be a number not: '${day}'`);
            }
            if (!month || !isNumber(month)) {
                errorMessages.push(`'month' should exist and be a number not: '${month}'`);
            }
            if (!year || !isNumber(year)) {
                errorMessages.push(`'year' should exist and be a number not: '${year}'`);
            }
        }
        return errorMessages;
    }

    private isNotInPast({year, month, day}: CheckDateBase, now: Date): string[] {
        const nowYear = now.getFullYear();
        const nowMonth = now.getMonth() + 1; // JS returns month as array index :)
        const nowDay = now.getDate();
        if (nowYear > year
            || (nowYear === year) && (nowMonth > month)
            || (nowYear === year) && (nowMonth === month) && nowDay > day) {
            return [`Check in date: year: ${year}, month: ${month}, day: ${day} is in the past. Now: ${now.toISOString()}.`];
        } else {
            return [];
        }
    }

    private validateDate(checkInDate: CheckDateBase, checkOutDate: CheckDateBase): string[] {
        const errorMessages = [];
        if (checkInDate.year > checkOutDate.year) {
            errorMessages.push(`'endDate.year': '${checkOutDate.year}' should be equal or bigger then 'startDate.year': ${checkInDate.year}`);
        }
        const endMonth = checkOutDate.month + ((checkOutDate.year - checkInDate.year) * 12);
        if (checkInDate.month > endMonth) {
            errorMessages.push(`'endDate.month': '${checkOutDate.month}' should be equal or bigger then 'startDate.month': ${checkInDate.month}`);
        }
        if (checkInDate.month === checkOutDate.month && checkInDate.day >= checkOutDate.day) {
            errorMessages.push(`'endDate.day': '${checkOutDate.day}' should be bigger then 'startDate.day': ${checkInDate.day}`);
        }
        return errorMessages;
    }

    private validateCurrencyAndLanguage({currency, language}: RawRequest): string[] {
        const errorMessages = [];
        if (!Currency[currency]) {
            errorMessages.push(`'currency' is an enum and could has only values like: ${JSON.stringify(Object.keys(Currency))}, ` +
                `not: '${currency}'.`);
        }
        if (!Language[language]) {
            errorMessages.push(`'language' is an enum and could has only values like: ${JSON.stringify(Object.keys(Language))}, ` +
                `not: '${language}'.`);
        }
        return errorMessages;
    }

    private concatErrorMessagesIntoOneMessage = (errorMessagesArr: string[]): string =>
        errorMessagesArr.slice(1).reduce((previous, current) => previous.concat(' | ' + current), errorMessagesArr[0])
}
