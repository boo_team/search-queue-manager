import {SearchRequestValidator} from './SearchRequestValidator';
import {SearchRequestRawBuilder} from './SearchRequestRawBuilder';
import {CheckDateBuilder} from './CheckDateBuilder';

describe('SearchRequestValidator: ', () => {

    let validator: SearchRequestValidator;

    beforeEach(() => {
        validator = new SearchRequestValidator();
    });

    describe('validate:', () => {

        it('should not throw error if every parameter is valid', () => {
            // Prepare
            const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                .build();

            // Verify
            expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 1))).not.toThrowError();
        });

        describe('city:', () => {

            it('should exist [1]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .city('')
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2017, 0, 1))).toThrowError();
            });

            it('should exist [2]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .city(null)
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2017, 0, 1))).toThrowError();
            });

            it('should exist [3]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .city(undefined)
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2017, 0, 1))).toThrowError();
            });
        });

        describe('time:', () => {
            it('should throw error if date values are not numbers [1]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .checkInDate((CheckDateBuilder.get() as any)
                        .year('2018')
                        .month(null)
                        .day(undefined)
                        .build())
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 1))).toThrowError();
            });

            it('should throw error if date values are not numbers [2]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .checkInDate((CheckDateBuilder.get() as any)
                        .year([])
                        .month({})
                        .day(new Date())
                        .build())
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 1))).toThrowError();
            });

            it('should throw error if check in date is in the past [1]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .checkInDate(CheckDateBuilder.get()
                        .year(2018)
                        .month(1)
                        .day(1)
                        .build())
                    .checkOutDate(CheckDateBuilder.get()
                        .year(2018)
                        .month(1)
                        .day(2)
                        .build())
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 3))).toThrowError();
            });

            it('should throw error if check in date is in the past [2]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .checkInDate(CheckDateBuilder.get()
                        .year(2019)
                        .month(1)
                        .day(2)
                        .build())
                    .checkOutDate(CheckDateBuilder.get()
                        .year(2019)
                        .month(1)
                        .day(3)
                        .build())
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2019, 0, 3))).toThrowError();
            });

            it('should throw error if the check in date is later then the check out date [1]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .checkInDate(CheckDateBuilder.get()
                        .year(2019)
                        .month(2)
                        .day(2)
                        .build())
                    .checkOutDate(CheckDateBuilder.get()
                        .year(2018)
                        .month(2)
                        .day(3)
                        .build())
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 1))).toThrowError();
            });

            it('should throw error if the check in date is later then the check out date [2]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .checkInDate(CheckDateBuilder.get()
                        .year(2018)
                        .month(3)
                        .day(2)
                        .build())
                    .checkOutDate(CheckDateBuilder.get()
                        .year(2018)
                        .month(2)
                        .day(2)
                        .build())
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 1))).toThrowError();
            });

            it('should throw error if the check in date is later then the check out date [3]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .checkInDate(CheckDateBuilder.get()
                        .year(2019)
                        .month(1)
                        .day(2)
                        .build())
                    .checkOutDate(CheckDateBuilder.get()
                        .year(2018)
                        .month(12)
                        .day(30)
                        .build())
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 1))).toThrowError();
            });

            it('should throw error if the check in date is later then the check out date [4]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .checkInDate(CheckDateBuilder.get()
                        .year(2018)
                        .month(2)
                        .day(2)
                        .build())
                    .checkOutDate(CheckDateBuilder.get()
                        .year(2018)
                        .month(2)
                        .day(2)
                        .build())
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 1))).toThrowError();
            });

            it('should throw error if the check in date is later then the check out date [5]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .checkInDate(CheckDateBuilder.get()
                        .year(2018)
                        .month(2)
                        .day(3)
                        .build())
                    .checkOutDate(CheckDateBuilder.get()
                        .year(2018)
                        .month(2)
                        .day(2)
                        .build())
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 1))).toThrowError();
            });
        });

        describe('value definition:', () => {
            it('should throw error if one of relevant values is undefined [1]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .priority(undefined)
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 1))).toThrowError();
            });

            it('should throw error if one of relevant values is null [2]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .checkOutDate(null)
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 1))).toThrowError();
            });
        });

        describe('number values:', () => {
            it('should throw error if one of numeric values is not a number [1]', () => {
                // Prepare
                const searchParamsRaw = (SearchRequestRawBuilder.fullyPopulatedBuilder() as any)
                    .updateFrequencyMinutes('180')
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 1))).toThrowError();
            });

            it('should throw error if one of numeric values is not a number [2]', () => {
                // Prepare
                const searchParamsRaw = (SearchRequestRawBuilder.fullyPopulatedBuilder() as any)
                    .numberOfAdults({})
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 1))).toThrowError();
            });

            it('should throw error if one of numeric values is not a number [3]', () => {
                // Prepare
                const searchParamsRaw = (SearchRequestRawBuilder.fullyPopulatedBuilder() as any)
                    .resultsLimit([])
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 1))).toThrowError();
            });
        });

        describe('validate currency and language:', () => {

            it('should throw error if is not an enum value [1]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .currency('WAW' as any)
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 1))).toThrowError();
            });

            it('should throw error if is not an enum value [2]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .language('' as any)
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 1))).toThrowError();
            });

            it('should throw error if is not an enum value [3]', () => {
                // Prepare
                const searchParamsRaw = SearchRequestRawBuilder.fullyPopulatedBuilder()
                    .language({} as any)
                    .build();

                // Verify
                expect(() => validator.validate(searchParamsRaw, new Date(2018, 0, 1))).toThrowError();
            });
        });
    });
});
