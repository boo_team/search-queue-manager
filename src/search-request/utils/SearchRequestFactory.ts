import {SearchRequestBase} from '../interface/base/searchRequest.base';
import {CheckDateBase} from '../interface/base/checkDate.base';
import {ChildrenPropertiesBase} from '../interface/base/childrenProperties.base';
import {SearchRequestRaw} from '../interface/searchRequest.raw';
import {OccupancyStatus} from '../enum/OccupancyStatus';

export class SearchRequestFactory {
    build(searchRequestRaw: SearchRequestRaw): SearchRequestBase {
        return new SearchRequestBuilder(searchRequestRaw);
    }
}

class SearchRequestBuilder implements SearchRequestBase {

    public readonly searchId: string;
    public readonly priority: number;
    public readonly updateFrequencyMinutes: number;
    public readonly resultsLimit: number;
    public readonly occupancyStatus: string;
    public readonly occupancyUpdatedAt: string;

    // Scenario parameters
    public readonly city: string;
    public readonly checkInDate: CheckDateBase;
    public readonly checkOutDate: CheckDateBase;
    public readonly numberOfRooms: number;
    public readonly numberOfAdults: number;
    public readonly numberOfChildren: ChildrenPropertiesBase[];
    public readonly currency: string;
    public readonly language: string;

    constructor({
                    priority,
                    updateFrequencyMinutes,
                    resultsLimit,
                    city,
                    checkInDate,
                    checkOutDate,
                    numberOfRooms,
                    numberOfAdults,
                    numberOfChildren,
                    currency,
                    language,
                }: SearchRequestRaw) {
        this.searchId = `${city.trim().toUpperCase().replace(/ /g, '_')}_` +
            `${checkInDate.year}-${checkInDate.month}-${checkInDate.day}_` +
            `${checkOutDate.year}-${checkOutDate.month}-${checkOutDate.day}_` +
            `${numberOfRooms}_${numberOfAdults}_${numberOfChildren.length}_` +
            `${priority}_${updateFrequencyMinutes}_${resultsLimit}_${currency}_${language}`;
        this.priority = priority;
        this.updateFrequencyMinutes = updateFrequencyMinutes;
        this.resultsLimit = resultsLimit;
        this.occupancyStatus = OccupancyStatus.FREE;
        this.occupancyUpdatedAt = null; // value will be updated after first use of this request
        this.city = city;
        this.checkInDate = checkInDate;
        this.checkOutDate = checkOutDate;
        this.numberOfRooms = numberOfRooms;
        this.numberOfAdults = numberOfAdults;
        this.numberOfChildren = numberOfChildren;
        this.currency = currency;
        this.language = language;
    }
}
