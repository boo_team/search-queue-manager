export interface CheckDateBase {
    readonly year: number;
    readonly month: number;
    readonly day: number;
}
