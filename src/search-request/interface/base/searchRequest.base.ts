import {CheckDateBase} from './checkDate.base';
import {ChildrenPropertiesBase} from './childrenProperties.base';

export interface SearchRequestBase {
    readonly searchId: string;
    readonly priority: number;
    readonly updateFrequencyMinutes: number;
    readonly resultsLimit: number;
    readonly occupancyStatus: string;
    readonly occupancyUpdatedAt: string;

    // Scenario parameters
    readonly city: string;
    readonly checkInDate: CheckDateBase;
    readonly checkOutDate: CheckDateBase;
    readonly numberOfRooms: number;
    readonly numberOfAdults: number;
    readonly numberOfChildren: ChildrenPropertiesBase[];
    readonly currency: string;
    readonly language: string;
}
