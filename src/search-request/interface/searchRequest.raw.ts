import {SearchRequestBase} from './base/searchRequest.base';

export interface SearchRequestRaw extends Omit<SearchRequestBase, 'searchId' | 'occupancyStatus' | 'occupancyUpdatedAt'> {
}
