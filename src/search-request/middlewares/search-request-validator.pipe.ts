import {ArgumentMetadata, BadRequestException, Injectable, PipeTransform} from '@nestjs/common';
import {logger} from '../../config';
import {SearchRequestRaw} from '../interface/searchRequest.raw';
import {SearchRequestDto} from '../dto/searchRequest.dto';
import {SearchRequestValidator} from '../utils/SearchRequestValidator';

@Injectable()
export class SearchRequestValidatorPipe implements PipeTransform {
    transform(value: any, metadata: ArgumentMetadata) {
        if (metadata.type === 'body') {
            const searchRequestValidator = new SearchRequestValidator();
            try {
                searchRequestValidator.validate(value as SearchRequestRaw | SearchRequestDto, new Date());
                return value;
            } catch (e) {
                logger.error('Request is invalid:', e);
                throw new BadRequestException(`Request is invalid: ${e.message}`);
            }
        } else {
            return value;
        }
    }
}
