import {Test, TestingModule} from '@nestjs/testing';
import {SearchRequestsController} from './search-requests.controller';
import {getModelToken} from '@nestjs/mongoose';
import {SearchRequestsRepository} from './search-requests.repository';
import {AuthClientService} from '../auth-client/auth-client.service';
import {SearchRequestRaw} from './interface/searchRequest.raw';
import {WebService} from '../web/web.service';
import {OwnedRequestsService} from './client/owned-requests.service';
import {SearchRequestsService} from './search-requests.service';
import { model, Schema } from 'mongoose';
import {SearchRequestDocument, SearchRequestSchema, SearchRequestSchemaKey, UserDocument, UserSchema, UserSchemaKey} from '@bo/common';

const userModel = model<UserDocument>(UserSchemaKey, UserSchema(Schema));
const searchRequestModel = model<SearchRequestDocument>(SearchRequestSchemaKey, SearchRequestSchema(Schema));

class SearchRequestsServiceMock {
    findOrCreate = () => Promise.resolve();
}

class WebServiceMock {
    sendSearchRequestsToUserClients = () => {
        return;
    }
}

describe('SearchRequestController', () => {

    let testModule: TestingModule;

    beforeEach(async () => {
        testModule = await Test.createTestingModule({
            controllers: [
                SearchRequestsController,
            ],
            providers: [
                AuthClientService,
                OwnedRequestsService,
                {provide: SearchRequestsService, useClass: SearchRequestsServiceMock},
                SearchRequestsRepository,
                {provide: WebService, useClass: WebServiceMock},
                {provide: getModelToken(SearchRequestSchemaKey), useValue: searchRequestModel},
                {provide: getModelToken(UserSchemaKey), useValue: userModel},
            ],
        }).compile();
    });

    describe('createSearchRequest()', () => {

        let searchRequestController: SearchRequestsController;
        let searchRequestRepository: SearchRequestsRepository;
        let searchRequestService: SearchRequestsService;
        let authClientService: AuthClientService;
        let webService: WebService;

        beforeEach(() => {
            searchRequestController = testModule.get<SearchRequestsController>(SearchRequestsController);
            searchRequestRepository = testModule.get<SearchRequestsRepository>(SearchRequestsRepository);
            searchRequestService = testModule.get<SearchRequestsService>(SearchRequestsService);
            authClientService = testModule.get<AuthClientService>(AuthClientService);
            webService = testModule.get<WebService>(WebService);
        });

        it('should create search request, save it to db, update user and notify all user clients', async () => {
            // GIVEN
            const userDocMock = {_id: 'mock' as any, ownedSearchRequests: []} as UserDocument;
            const searchRequestRawMock = {
                city: 'mock',
                checkInDate: {},
                checkOutDate: {},
                numberOfChildren: [],
            } as SearchRequestRaw;

            jest.spyOn(authClientService, 'findUserById').mockReturnValue(Promise.resolve(userDocMock));
            jest.spyOn(authClientService, 'findByIdAndUpdateOwnedReq').mockReturnValue(Promise.resolve(userDocMock));
            jest.spyOn(searchRequestService, 'findOrCreate')
                .mockReturnValue(Promise.resolve({searchId: 'mock', ...searchRequestRawMock} as SearchRequestDocument));
            jest.spyOn(searchRequestRepository, 'getAllFromUser')
                .mockReturnValue(Promise.resolve([{searchId: 'mock'} as SearchRequestDocument]));
            jest.spyOn(webService, 'sendSearchRequestsToUserClients');

            // WHEN
            const result = await searchRequestController.createSearchRequest('mock', searchRequestRawMock);

            // THEN
            expect(authClientService.findByIdAndUpdateOwnedReq).lastCalledWith('mock', [{
                searchId: 'mock',
                favorites: [],
            }]);
            expect(webService.sendSearchRequestsToUserClients).lastCalledWith(userDocMock, [{searchId: 'mock'}]);
            expect(result.searchId).toBe('mock');
        });
    });
});
