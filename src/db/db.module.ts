import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {MONGO_ADDRESS} from '../config';
import {
    RawSearchResultSchema,
    RawSearchResultSchemaKey,
    SearchRequestSchema,
    SearchRequestSchemaKey,
    UserSchema,
    UserSchemaKey,
} from '@bo/common';
import { Schema } from "mongoose";

const mongooseModules = [
    MongooseModule.forRoot(MONGO_ADDRESS, {useNewUrlParser: true}),
    MongooseModule.forFeature([{name: SearchRequestSchemaKey, schema: SearchRequestSchema(Schema)}]),
    MongooseModule.forFeature([{name: RawSearchResultSchemaKey, schema: RawSearchResultSchema(Schema)}]),
    MongooseModule.forFeature([{name: UserSchemaKey, schema: UserSchema(Schema)}]),
];

@Module({
    imports: [
        ...mongooseModules,
    ],
    exports: [
        ...mongooseModules,
    ],
})
export class DbModule {
}
