import {Module} from '@nestjs/common';
import {logger} from '../config';
import {SocketClient} from '@bo/common';

@Module({
    providers: [
        {
            provide: SocketClient,
            useFactory: () => new SocketClient(logger),
        },
    ],
    exports: [
        SocketClient,
    ],
})
export class StatusModule {
}
