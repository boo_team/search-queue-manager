import {LoggerService} from '@nestjs/common';
import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {logger, PORT} from './config';

async function bootstrap() {
    const app = await NestFactory.create(AppModule, {
        logger: logger as LoggerService,
    });
    app.enableCors(); // TODO: add proper cors options - http://localhost:4200'
    await app.listen(PORT);
}

bootstrap();
