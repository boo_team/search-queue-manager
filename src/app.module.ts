import {Module} from '@nestjs/common';
import {SearchRequestsModule} from './search-request/search-requests.module';
import {AuthModule} from './auth/auth.module';
import {StatusModule} from './status/status.module';
import {SearchResultModule} from './search-result/search-result.module';
import {AuthClientModule} from './auth-client/auth-client.module';
import {DbModule} from './db/db.module';

@Module({
    imports: [
        SearchRequestsModule,
        SearchResultModule,
        StatusModule,
        AuthModule,
        AuthClientModule,
        DbModule,
    ],
})
export class AppModule {
}
