import {Role} from '../model/Role';
import {OwnedRequestDto} from './ownedRequestDto';

export interface UserDto {
    email: string;
    ownedSearchRequests: OwnedRequestDto[];
    roles: Role[];
}
