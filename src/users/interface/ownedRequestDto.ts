export interface OwnedRequestDto {
    readonly searchId: string;
    readonly favorites: string[];
}
