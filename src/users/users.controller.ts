import {Body, ConflictException, Controller, Delete, Get, Headers, HttpStatus, Param, Patch, Post} from '@nestjs/common';
import {UsersRepository} from './users-repository';
import {Roles} from '../common/decorator/roles.decorator';
import {NoConstraints} from '../common/decorator/noConstraints.decorator';
import {Role} from './model/Role';
import {CredentialsRaw} from '../common/interface/credentials.raw';
import {extractTokenWithBearerPayload} from '../common/utils/extractPayload';
import {decodeString} from '../common/utils/decodeString';
import {UserDto} from './interface/user.dto';
import {UserMapper} from './utils/UserMapper';
import {logger} from '../config';
import { UserDocument } from '@bo/common';

@Controller('api/v1/users')
export class UsersController {

    constructor(private readonly usersRepository: UsersRepository) {
    }

    @Get(':encodedEmail')
    @Roles(Role.USER)
    public async get(@Param('encodedEmail') encodedEmail: string): Promise<UserDto> {
        const decodedEmail = decodeString(encodedEmail);
        const userDoc = await this.usersRepository.findByEmail(decodedEmail);
        return UserMapper.fromDocToDto(userDoc);
    }

    @Post()
    @NoConstraints()
    public async createUser(@Body() {email, password}: CredentialsRaw): Promise<UserDto> {
        const found = await this.usersRepository.findByEmail(email);
        if (found) {
            throw new ConflictException(`User with email: '${email}' already exist`);
        } else {
            const created = await this.usersRepository.createAndAssignDefaultAccessRights({email, password});
            logger.info(`User [${email}] created.`);
            return UserMapper.fromDocToDto(created);
        }
    }

    @Post('favorites/:searchId')
    @Roles(Role.USER)
    public async updateFavorites(@Headers('authorization') bearerWithToken: string,
                                 @Param('searchId') searchId: string,
                                 @Body() {favorites}: { favorites: string[] }): Promise<UserDto> {
        const {id} = extractTokenWithBearerPayload(bearerWithToken);
        const userDoc = await this.usersRepository.findByIdAndUpdateFavorites(id, searchId, favorites);
        return UserMapper.fromDocToDto(userDoc);
    }

    @Patch()
    @Roles(Role.USER)
    public async updateUser(@Headers('authorization') bearerWithToken: string,
                            @Body() partialUpdate: Partial<UserDocument>): Promise<UserDto> {
        const {id} = extractTokenWithBearerPayload(bearerWithToken);
        const userDoc = await this.usersRepository.findByIdAndPartialUpdate(id, partialUpdate);
        return UserMapper.fromDocToDto(userDoc);
    }

    @Patch(':encodedEmail')
    @Roles(Role.ADMIN)
    public async updateUserWithAdminRights(@Param('encodedEmail') encodedEmail: string,
                                           @Body() partialUpdate: Partial<UserDocument>): Promise<UserDto> {
        const decodedEmail = decodeString(encodedEmail);
        const userDoc = await this.usersRepository.findByEmailAndUpdate(decodedEmail, partialUpdate);
        return UserMapper.fromDocToDto(userDoc);
    }

    @Get()
    @Roles(Role.ADMIN)
    public async getAllUsers(): Promise<Array<UserDto>> {
        const allUsers = await this.usersRepository.findAll();
        return allUsers.map(userDoc => UserMapper.fromDocToDto(userDoc));
    }

    @Delete()
    @Roles(Role.ADMIN)
    public async deleteUser(@Body() credentialsRaw: CredentialsRaw): Promise<HttpStatus> {
        const userDoc = await this.usersRepository.findByCredentialsOrFail(credentialsRaw);
        await this.usersRepository.deleteById(userDoc.id);
        return HttpStatus.OK;
    }
}
