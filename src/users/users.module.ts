import {Module} from '@nestjs/common';
import {UsersController} from './users.controller';
import {UsersRepository} from './users-repository';
import {DbModule} from '../db/db.module';

@Module({
    imports: [
        DbModule,
    ],
    providers: [
        UsersRepository,
    ],
    exports: [
        UsersRepository,
    ],
    controllers: [UsersController],
})
export class UsersModule {

}
