import {Injectable, UnauthorizedException} from '@nestjs/common';
import {Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import {hashPassword} from './utils/hashPassword';
import {CredentialsRaw} from '../common/interface/credentials.raw';
import {Role} from './model/Role';
import {UserDocument, UserSchemaKey} from '@bo/common';

@Injectable()
export class UsersRepository {
    constructor(@InjectModel(UserSchemaKey) private readonly userModel: Model<UserDocument>) {
    }

    public findAll(): Promise<UserDocument[]> {
        return this.userModel.find().exec();
    }

    public findByIdOrFail(id: string): Promise<UserDocument> {
        return this.userModel.findById(id)
            .orFail(() => new UnauthorizedException(`User not exist.`)).exec();
    }

    public findByEmail(email: string): Promise<UserDocument> {
        return this.userModel.findOne({email}).exec();
    }

    public findByIdAndUpdateFavorites(id: string, searchId: string, favorites: string[]): Promise<UserDocument> {
        return this.userModel.findOneAndUpdate(
            {'_id': id, 'ownedSearchRequests.searchId': searchId},
            {$set: {'ownedSearchRequests.$.favorites': favorites}},
            {new: true}).exec();
    }

    public findByIdAndPartialUpdate(id: string, partialUpdate: Partial<UserDocument>): Promise<UserDocument> {
        const update = partialUpdate.password
            ? {...partialUpdate, password: hashPassword(partialUpdate.password)}
            : partialUpdate;
        return this.userModel.findByIdAndUpdate(id, update, {new: true}).exec();
    }

    public findByEmailAndUpdate(email: string, update: Partial<UserDocument>): Promise<UserDocument> {
        return this.userModel.findOneAndUpdate({email}, update, {new: true}).exec();
    }

    public deleteById(id: string): Promise<UserDocument> {
        return this.userModel.findByIdAndDelete(id).exec();
    }

    public findByCredentialsOrFail({email, password}: CredentialsRaw): Promise<UserDocument> {
        return this.userModel.findOne({
            email,
            password: hashPassword(password),
        })
            .orFail(() => new UnauthorizedException('Email not found or invalid password'))
            .exec();
    }

    public createAndAssignDefaultAccessRights({email, password}: CredentialsRaw): Promise<UserDocument> {
        return new this.userModel({
            email,
            password: hashPassword(password),
            ownedSearchRequests: [],
            roles: [Role.USER],
            requestsLimit: 5,
        } as UserDocument).save();
    }
}
