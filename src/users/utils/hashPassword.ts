import * as crypto from 'crypto';
import {SALT} from '../../config';

export function hashPassword(password: string) {
    return crypto.createHmac('sha256', SALT)
        .update(password)
        .digest('hex');
}
