import {UserDto} from '../interface/user.dto';
import {UserDocument} from '@bo/common';
import {Role} from '../model/Role';

export class UserMapper {
    public static fromDocToDto({
                                   email,
                                   ownedSearchRequests,
                                   roles,
                               }: UserDocument): UserDto {
        return {
            email,
            ownedSearchRequests,
            roles: roles as Role[],
        };
    }
}
