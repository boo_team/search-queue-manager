import {Injectable} from '@nestjs/common';
import {logger, STATUS_MONITOR_ADDRESS} from '../config';
import {WebGateway} from './web.gateway';
import {SearchRequestDocument, UserDocument, SocketClient, StatusEvent, ProcessingResultStatusDto, ScrapingStatusDto} from '@bo/common';
import {Socket} from 'socket.io';
import {Observable} from 'rxjs';
import {ConnectedEvent} from './interfaces/connected.event';

@Injectable()
export class WebService {
    constructor(
        private readonly socketClient: SocketClient,
        private readonly webGateway: WebGateway,
    ) {
        this.watchEvents();
    }

    public sendSearchRequestsToUserClients(userDoc: UserDocument, userRequestsDocs: SearchRequestDocument[]): void {
        const clients = this.webGateway.getUserClients(userDoc);
        clients.forEach((client) => this.webGateway.sendSearchRequestsToClient(client, userRequestsDocs));
    }

    public sendSearchRequestsToClient(client: Socket, userRequestsDocs: SearchRequestDocument[]): void {
        this.webGateway.sendSearchRequestsToClient(client, userRequestsDocs);
    }

    private watchEvents(): void {
        this.socketClient.watchEvent<ProcessingResultStatusDto>(StatusEvent.PROCESSING_RESULT_STATUS)
            .subscribe((dto: ProcessingResultStatusDto) => {
                logger.debug(`[${StatusEvent.PROCESSING_RESULT_STATUS}] event received from status monitor. Dto:`, dto);
                this.webGateway.sendIfConnected<ProcessingResultStatusDto>(StatusEvent.PROCESSING_RESULT_STATUS, dto);
            });
        this.socketClient.watchEvent<ScrapingStatusDto>(StatusEvent.SCRAPING_STATUS)
            .subscribe((dto: ScrapingStatusDto) => {
                logger.debug(`[${StatusEvent.SCRAPING_STATUS}] event received from status monitor. Dto:`, dto);
                this.webGateway.sendIfConnected<ScrapingStatusDto>(StatusEvent.SCRAPING_STATUS, dto);
            });
        this.webGateway.clientConnected$().subscribe(() => {
            if (!this.socketClient.isConnected()) {
                logger.debug('Trying to connect to status monitor');
                this.socketClient.connect(`${STATUS_MONITOR_ADDRESS}/status`);
            }
        });
        this.webGateway.clientDisconnected$().subscribe((isAnyConnections: boolean) => {
            if (!isAnyConnections && this.socketClient.isConnected()) {
                logger.debug('Trying to disconnect from status monitor');
                this.socketClient.disconnect();
            }
        });
    }

    public clientConnected$ = (): Observable<ConnectedEvent> => this.webGateway.clientConnected$();
}
