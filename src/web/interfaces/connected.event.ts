import {Socket} from 'socket.io';
import {UserDocument} from '@bo/common';

export interface ConnectedEvent {
    readonly client: Socket;
    readonly userDoc: UserDocument;
}
