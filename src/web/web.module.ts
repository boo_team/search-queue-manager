import {Module} from '@nestjs/common';
import {WebGateway} from './web.gateway';
import {StatusModule} from '../status/status.module';
import {WebService} from './web.service';
import {AuthClientModule} from '../auth-client/auth-client.module';

@Module({
    providers: [
        WebGateway,
        WebService,
    ],
    imports: [
        StatusModule,
        AuthClientModule,
    ],
    exports: [
        WebService,
    ],
})
export class WebModule {
}
