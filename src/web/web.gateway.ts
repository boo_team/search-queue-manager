import {OnGatewayConnection, OnGatewayDisconnect, WebSocketGateway, WebSocketServer} from '@nestjs/websockets';
import {Namespace, Socket} from 'socket.io';
import {logger} from '../config';
import {SearchRequestMapper} from '../search-request/utils/SearchRequestMapper';
import {SocketEvent} from '../search-request/enum/SocketEvent';
import {SearchRequestDocument, UserDocument, StatusEvent} from '@bo/common';
import {Observable, ReplaySubject} from 'rxjs';
import {extractTokenPayload} from '../common/utils/extractPayload';
import {AuthClientService} from '../auth-client/auth-client.service';
import {ConnectedEvent} from './interfaces/connected.event';

@WebSocketGateway({namespace: '/web'})
export class WebGateway implements OnGatewayConnection, OnGatewayDisconnect {

    @WebSocketServer()
    private readonly nsps: Namespace;

    private readonly onClientConnectedSubject: ReplaySubject<ConnectedEvent>;
    private readonly onClientDisconnectedSubject: ReplaySubject<boolean>;

    constructor(
        private readonly authClientService: AuthClientService,
    ) {
        this.onClientConnectedSubject = new ReplaySubject(1);
        this.onClientDisconnectedSubject = new ReplaySubject(1);
    }

    public sendSearchRequestsToClient(target: Socket, searchRequestDocs: SearchRequestDocument[]): void {
        const searchRequestsDto = searchRequestDocs.map((v) => SearchRequestMapper.fromDocToDto(v));
        target.emit(SocketEvent.SEARCH_REQUESTS, searchRequestsDto);
        logger.debug(`All search requests (size: ${searchRequestsDto.length}) are sent to: ${target.id}.`);
    }

    // override
    async handleConnection(client: Socket): Promise<void> {
        logger.debug(`Connected to web client with id [${client.id}]`);
        const query = client.request._query;
        try {
            const userDoc = await this.authClientService.verifyTokenAndGetUser(query);
            this.onClientConnectedSubject.next({client, userDoc});
        } catch (err) {
            logger.error(`Web client with id [${client.id}] has invalid token. `, err);
            client.emit('unauthorized_error', (err as Error).message);
            client.disconnect(true);
        }
    }

    // override
    async handleDisconnect(client: Socket): Promise<void> {
        logger.debug('Disconnected from web client with id: ', client.id);
        this.onClientDisconnectedSubject.next(this.ifAnyClientConnected(this.nsps));
    }

    public getUserClients(userDoc: UserDocument): Socket[] {
        if (this.ifAnyClientConnected(this.nsps)) {
            return Object.values(this.nsps.connected).filter((socket) => {
                const token = socket.request._query.token;
                const userId = extractTokenPayload(token).id;
                return userDoc._id.equals(userId);
            });
        } else {
            return [];
        }
    }

    public sendIfConnected<T>(event: StatusEvent, dto: T): void {
        if (this.ifAnyClientConnected(this.nsps)) {
            this.nsps.emit(event, dto);
            logger.debug(`[${event}] event sent to all web clients with ids ${JSON.stringify(Object.keys(this.nsps.connected))}.` +
                `Search status: `, dto);
        }
    }

    public clientConnected$ = (): Observable<ConnectedEvent> => this.onClientConnectedSubject.asObservable();
    public clientDisconnected$ = (): Observable<boolean> => this.onClientDisconnectedSubject.asObservable();

    private ifAnyClientConnected = (nsps: Namespace): boolean => nsps && Object.keys(nsps.connected).length > 0;
}
