import {Logger} from '@bo/common';
import * as Winston from 'winston';
import {transports} from 'winston';

//// BASIC OPTIONS
const IS_PRODUCTION: boolean = process.env.NODE_ENV === 'production';
const SERVICE_LABEL = process.env.SERVICE_LABEL || 'SQM';
export const PORT = parseInt(process.env.PORT, 10) || 8080;
export const MONGO_ADDRESS = process.env.MONGO_ADDRESS || 'mongodb://127.0.0.1:27017/test';
export const STATUS_MONITOR_ADDRESS = process.env.STATUS_MONITOR_ADDRESS || 'http://127.0.0.1:8000';

//// TOKEN OPTIONS
export const SALT = process.env.SALT || 'HARD_TO_DECODE';
export const SECRET = process.env.SECRET || 'HARD_TO_DECODE';
export const TOKEN_EXPIRATION_TIME_SEC = parseInt(process.env.TOKEN_EXPIRATION_TIME_SEC, 10) || 60 * 60 * 24 * 7; // 7 days
export const DOMAIN = process.env.DOMAIN || `http://127.0.0.1:${PORT}`;
export const ISSUER = process.env.ISSUER || 'https://bitbucket.org/boo_team/';

//// LOG OPTIONS
const LOG_LEVEL = process.env.LOG_LEVEL || 'info';
const EXPANDED_VIEW_OF_JSON_IN_LOGS = process.env.EXPANDED_VIEW_OF_JSON_IN_LOGS === 'true';
const COLOR_LOGS = process.env.COLOR_LOGS === 'true';

const additionalTransports = [];
if (!IS_PRODUCTION) {
    additionalTransports.push(new transports.File({
        filename: 'logs/error.log',
        level: 'error',
    }));
    additionalTransports.push(new transports.File({
        filename: 'logs/combined.log',
        level: 'debug',
    }));
}

export const logger = Logger.get(Winston,
    `${SERVICE_LABEL}:${PORT}`, additionalTransports, LOG_LEVEL, EXPANDED_VIEW_OF_JSON_IN_LOGS, COLOR_LOGS);

logger.debug('Start logging with app setup: ', {
    IS_PRODUCTION,
    SERVICE_LABEL,
    PORT,
    MONGO_ADDRESS,
    STATUS_MONITOR_ADDRESS,
    TOKEN_EXPIRATION_TIME_SEC,
    DOMAIN,
    ISSUER,
    LOG_LEVEL,
    EXPANDED_VIEW_OF_JSON_IN_LOGS,
    COLOR_LOGS,
});

if (IS_PRODUCTION) {
    logger.debug('Google cloud setup: ', {
        GAE_APPLICATION: process.env.GAE_APPLICATION, // The ID of your App Engine application.
        GAE_DEPLOYMENT_ID: process.env.GAE_DEPLOYMENT_ID, // The ID of the current deployment.
        GAE_ENV: process.env.GAE_ENV, // The App Engine environment. Set to standard.
        GAE_INSTANCE: process.env.GAE_INSTANCE, // The ID of the instance on which your service is currently running.
        GAE_MEMORY_MB: process.env.GAE_MEMORY_MB, // The amount of memory available to the application process, in MB.
        GAE_RUNTIME: process.env.GAE_RUNTIME, // The runtime specified in your app.yaml file. The value is nodejs8 for Node.js.
        GAE_SERVICE: process.env.GAE_SERVICE, // The service name specified in your app.yaml file. If no service name is specified, it is set to default.
        GAE_VERSION: process.env.GAE_VERSION, // The current version label of your service.
        GOOGLE_CLOUD_PROJECT: process.env.GOOGLE_CLOUD_PROJECT, // The GCP project ID associated with your application.
        NODE_ENV: process.env.NODE_ENV, // Set to production when your service is deployed.
        PORT: process.env.PORT, // The port that receives HTTP requests. Set to 8080.
    });
}
