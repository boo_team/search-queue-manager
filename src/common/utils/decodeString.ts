export const decodeString = (encodedString: string): string => Buffer.from(encodedString, 'base64').toString('UTF-8');
