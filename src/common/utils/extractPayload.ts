import * as jwt from 'jsonwebtoken';
import {JwtPayload} from '../../auth/interface/jwtPayload';

export const extractTokenPayload = (token: string): JwtPayload => {
    return (jwt.decode(token) as JwtPayload);
};

export const extractTokenWithBearerPayload = (bearerWithToken: string): JwtPayload => {
    const token = bearerWithToken.split(' ')[1];
    return (jwt.decode(token) as JwtPayload);
};
