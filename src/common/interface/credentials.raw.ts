export interface CredentialsRaw {
    email: string;
    password: string;
}
