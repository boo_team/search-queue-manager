import {SetMetadata} from '@nestjs/common';

export const LIMITED_KEY = 'limited';
export const Limited = () => SetMetadata(LIMITED_KEY, true);
