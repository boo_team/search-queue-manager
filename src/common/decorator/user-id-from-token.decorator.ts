import {createParamDecorator} from '@nestjs/common';
import {Request} from 'express';
import {extractTokenWithBearerPayload} from '../utils/extractPayload';

// TODO: TEST IT!!
export const UserIdFromToken = createParamDecorator((data, req: Request) => {
    const bearerWithToken = req.headers['authorization'];
    return extractTokenWithBearerPayload(bearerWithToken).id;
});
