import {SetMetadata} from '@nestjs/common';

export const NO_CONSTRAINTS_KEY = 'noConstraints';
export const NoConstraints = () => SetMetadata(NO_CONSTRAINTS_KEY, true);
