import {Injectable, UnauthorizedException} from '@nestjs/common';
import {Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import {extractTokenPayload} from '../common/utils/extractPayload';
import * as jwt from 'jsonwebtoken';
import {SECRET} from '../config';
import {ObjectId} from 'bson';
import {OwnedRequestDocument, UserDocument, UserSchemaKey} from '@bo/common';

@Injectable()
export class AuthClientService {
    constructor(@InjectModel(UserSchemaKey) private readonly userModel: Model<UserDocument>) {
    }

    // TODO: it should call auth service if will be created
    public async verifyTokenAndGetUser(query: { token: string }): Promise<UserDocument> {
        if (!query || !query.token) {
            throw new Error(`Socket client need to contain auth token in query.`);
        }
        const token = query.token;
        await new Promise((resolve, reject) => {
            jwt.verify(token, SECRET, (err: jwt.VerifyErrors, decoded: object) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(decoded);
                }
            });
        });
        const _id = extractTokenPayload(token).id;
        return this.userModel.findById(_id)
            .orFail(() => new UnauthorizedException(`User not exist.`)).exec();
    }

    public findUserById(userId: string): Promise<UserDocument> {
        return this.userModel.findById(userId)
            .orFail(() => new UnauthorizedException(`User not exist.`)).exec();
    }

    public findByIdAndUpdateOwnedReq(id: ObjectId, ownedSearchRequests: OwnedRequestDocument[]): Promise<UserDocument> {
        return this.userModel.findByIdAndUpdate(id, {ownedSearchRequests}, {new: true}).exec();
    }

    public findOtherUsersWhoOwnReq(id: ObjectId, searchId: string): Promise<UserDocument[]> {
        return this.userModel.find({'_id': {$ne: id}, 'ownedSearchRequests.searchId': searchId}).exec();
    }

    public findOwnersOfSearchId(searchId: string): Promise<UserDocument[]> {
        return this.userModel.find({'ownedSearchRequests.searchId': searchId}).exec();
    }
}
