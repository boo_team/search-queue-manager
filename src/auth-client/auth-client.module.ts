import {Module} from '@nestjs/common';
import {AuthClientService} from './auth-client.service';
import {DbModule} from '../db/db.module';

@Module({
    imports: [
        DbModule,
    ],
    providers: [
        AuthClientService,
    ],
    exports: [
        AuthClientService,
    ],
})
export class AuthClientModule {
}
