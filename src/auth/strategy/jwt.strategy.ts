import {ExtractJwt, Strategy} from 'passport-jwt';
import {PassportStrategy} from '@nestjs/passport';
import {Injectable} from '@nestjs/common';
import {AuthService} from '../auth.service';
import {DOMAIN, ISSUER, SECRET} from '../../config';
import {JwtPayload} from '../interface/jwtPayload';
import {UsersRepository} from '../../users/users-repository';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    constructor(private readonly authService: AuthService,
                private readonly usersRepository: UsersRepository) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: SECRET,
            issuer: ISSUER,
            audience: DOMAIN,
            passReqToCallback: true,
        });
    }

    async validate(request, {id}: JwtPayload, done) {
        const user = await this.usersRepository.findByIdOrFail(id);
        request.user = user;
        done(null, user);
    }
}
