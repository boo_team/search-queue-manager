import {Module} from '@nestjs/common';
import {AuthController} from './auth.controller';
import {AuthService} from './auth.service';
import {JwtStrategy} from './strategy/jwt.strategy';
import {APP_GUARD} from '@nestjs/core';
import {AuthGuard} from './guard/auth.guard';
import {UsersModule} from '../users/users.module';

@Module({
    imports: [UsersModule],
    controllers: [AuthController],
    providers: [
        AuthService,
        JwtStrategy,
        {
            provide: APP_GUARD,
            useClass: AuthGuard,
        },
    ],
})
export class AuthModule {
}
