import {ExecutionContext, Injectable, NotAcceptableException} from '@nestjs/common';
import {AuthGuard as PassportGuard} from '@nestjs/passport';
import {Reflector} from '@nestjs/core';
import {ROLES_KEY} from '../../common/decorator/roles.decorator';
import {NO_CONSTRAINTS_KEY} from '../../common/decorator/noConstraints.decorator';
import {LIMITED_KEY} from '../../common/decorator/limited.decorator';
import {ExtendedRequest} from '../interface/extendedRequest';
import {Role} from '../../users/model/Role';
import {OwnedRequestDocument} from '@bo/common';

@Injectable()
export class AuthGuard extends PassportGuard('jwt') {

    constructor(private readonly reflector: Reflector) {
        super();
    }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const noConstraints = this.reflector.get<boolean>(NO_CONSTRAINTS_KEY, context.getHandler());
        if (noConstraints) {
            return true;
        }
        await super.canActivate(context); // after that execution a request has user document attached
        return this.validateConstraints(context);
    }

    private validateConstraints(context: ExecutionContext): boolean {
        const rolesWithAccess = this.reflector.get<Role[]>(ROLES_KEY, context.getHandler()) || [];
        const limited = this.reflector.get<boolean>(LIMITED_KEY, context.getHandler());
        const {user} = (context.switchToHttp().getRequest() as ExtendedRequest);
        const hasRightRole = this.hasRightRole(rolesWithAccess, user.roles as Role[]);
        const notExceededReqLimit = this.notExceededReqLimit(limited, user.requestsLimit, user.ownedSearchRequests);
        return hasRightRole && notExceededReqLimit;
    }

    private hasRightRole(rolesWithAccess: Role[], userRoles: Role[]): boolean {
        if (!rolesWithAccess.length) {
            return true;
        } else {
            return userRoles.some((r) => rolesWithAccess.indexOf(r) > -1);
        }
    }

    private notExceededReqLimit(limited: boolean, requestsLimit: number, ownedSearchRequests: OwnedRequestDocument[]): boolean {
        if (limited && ownedSearchRequests.length >= requestsLimit) {
            throw new NotAcceptableException(`The requests limit [${requestsLimit}] has been exceeded.`);
        } else {
            return true;
        }
    }
}
