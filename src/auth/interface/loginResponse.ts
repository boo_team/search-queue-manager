import {UserDto} from '../../users/interface/user.dto';

export interface LoginResponse {
    user: UserDto;
    accessToken: string;
}
