import { UserDocument } from '@bo/common';

export interface ExtendedRequest extends Request {
    user: UserDocument;
}
