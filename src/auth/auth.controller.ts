import {Body, Controller, Get, Headers, Post} from '@nestjs/common';
import {AuthService} from './auth.service';
import {CredentialsRaw} from '../common/interface/credentials.raw';
import {NoConstraints} from '../common/decorator/noConstraints.decorator';
import {LoginResponse} from './interface/loginResponse';
import {UserDto} from '../users/interface/user.dto';

@Controller('api/v1/auth')
export class AuthController {

    constructor(private readonly authService: AuthService) {
    }

    @Get('verify')
    @NoConstraints()
    public verifyToken(@Headers('authorization') bearerWithToken: string): Promise<UserDto> {
        return this.authService.verifyToken(this.extractToken(bearerWithToken));
    }

    @Post('token')
    @NoConstraints()
    public getToken(@Body() credentialsRaw: CredentialsRaw): Promise<LoginResponse> {
        return this.authService.createToken(credentialsRaw);
    }

    private extractToken = (bearerWithToken: string): string => {
        if (bearerWithToken) {
            const split = bearerWithToken.split(' ');
            return split && split[1]
                ? split[1]
                : null;
        } else {
            return null;
        }
    }
}
