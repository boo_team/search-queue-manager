import * as jwt from 'jsonwebtoken';
import {Injectable, UnauthorizedException} from '@nestjs/common';
import {UsersRepository} from '../users/users-repository';
import {DOMAIN, ISSUER, logger, SECRET, TOKEN_EXPIRATION_TIME_SEC} from '../config';
import {CredentialsRaw} from '../common/interface/credentials.raw';
import {LoginResponse} from './interface/loginResponse';
import {UserDto} from '../users/interface/user.dto';
import {UserMapper} from '../users/utils/UserMapper';
import {JwtPayload} from './interface/jwtPayload';

@Injectable()
export class AuthService {
    constructor(private readonly usersRepository: UsersRepository) {
    }

    public async verifyToken(token): Promise<UserDto> {
        const {id} = await new Promise((resolve) => {
            jwt.verify(token, SECRET, (err: jwt.VerifyErrors, decoded: JwtPayload) => {
                if (err) {
                    logger.error('Invalid token: ', err);
                    throw new UnauthorizedException('Invalid token');
                } else {
                    resolve(decoded);
                }
            });
        });
        const userDoc = await this.usersRepository.findByIdOrFail(id);
        return UserMapper.fromDocToDto(userDoc);
    }

    public async createToken(credentialsRaw: CredentialsRaw): Promise<LoginResponse> {
        const userDoc = await this.usersRepository.findByCredentialsOrFail(credentialsRaw);
        const accessToken = jwt.sign({id: userDoc._id}, SECRET,
            {
                expiresIn: TOKEN_EXPIRATION_TIME_SEC,
                audience: DOMAIN,
                issuer: ISSUER,
            });
        const userDto = UserMapper.fromDocToDto(userDoc);
        return {user: userDto, accessToken};
    }
}
